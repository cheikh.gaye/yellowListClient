Feature: Testing SmtpErrorTypes webpage

  Scenario: User navigates to smtpErrorType page
    Given The use is on StmpErroType List page "http://localhost:8080/#/dashboard"
    Then  The page's title should be "Yellow List"
    When  The user clicks on the button with id "smtp-error-type"
    Then  The go to new page "http://localhost:8080/#/dashboard"