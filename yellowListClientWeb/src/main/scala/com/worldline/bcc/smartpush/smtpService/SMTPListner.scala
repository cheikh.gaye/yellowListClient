package com.worldline.bcc.smartpush.smtpService

import com.datastax.oss.driver.api.core.CqlSession
import com.worldline.bcc.smartpush.configs.AppConfig
import com.worldline.bcc.smartpush.models.SmtpErrorModel
import com.worldline.bcc.smartpush.services.{SmtpErrorService, SmtpErrorTypeService}
import io.vertx.core.Vertx
import io.vertx.kafka.client.consumer.{KafkaConsumer, KafkaConsumerRecord}
import com.worldline.bcc.smartpush.utils.DateUtils
import io.vertx.core.impl.logging.LoggerFactory

import java.time.LocalDateTime
import java.util.{Properties, UUID}


object SMTPListner {
  val logger = LoggerFactory.getLogger(getClass)
  private var smtpErrorDAO: SmtpErrorService = _
  private var smtpErrorTypeDAO: SmtpErrorTypeService = _

  val vertx = Vertx.vertx()

  def run() = {
    val cqlSession = CqlSession.builder().build()
    smtpErrorDAO = new SmtpErrorService(vertx, cqlSession)
    smtpErrorTypeDAO = new SmtpErrorTypeService(vertx, cqlSession)

    val consumerConfig = new Properties()
    consumerConfig.put("bootstrap.servers", AppConfig.kafkaBrockerAddress)
    consumerConfig.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer")
    consumerConfig.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer")
    consumerConfig.put("group.id", AppConfig.consumerGroup)

    val consumer = KafkaConsumer.create[String, String](vertx, consumerConfig)
    val topic = AppConfig.kafkaTopic

    consumer.handler { (record: KafkaConsumerRecord[String, String]) =>
      // Handle Kafka records here
      val id = UUID.randomUUID()
      val email = record.key()
      val error_code_rec = record.value()
      val date = DateUtils.convertLocalDateToString(LocalDateTime.now())
      try {
        if (!smtpErrorDAO.isEmailPresentInDatabase(email)) {
          val smtpErrorTypeModel = smtpErrorTypeDAO.findByErrorCodeReception(error_code_rec).result().get.id
          //UUID.fromString("9952b5e8-7deb-46e6-a0d5-551c40af1ac0")
          val smtpErrorModel = SmtpErrorModel(id = id, email = email, creationDate = date, lastAttemptDate = date, nbKo = 1, koTypeId = smtpErrorTypeModel)
          smtpErrorDAO.addSmtpError(smtpErrorModel)
        } else {
          val existingSmtpErrorModel = smtpErrorDAO.getSmtpErrorByEmail(email)
          val updatedSmtpErrorModel = existingSmtpErrorModel.get.copy(
            lastAttemptDate = date,
            nbKo = existingSmtpErrorModel.get.nbKo + 1
          )
          smtpErrorDAO.UpdateSmtpError(updatedSmtpErrorModel)
          //logger.warn("Le message aurait dû être bloqué, donc erreur interne")
        }
      } catch {
        case e: Exception =>
          e.printStackTrace()
      }
    }
    // Subscribe to the Kafka topic
    consumer.subscribe(topic)
  }
}
