package com.worldline.bcc.smartpush.beans

import java.util.UUID

class SmtpErrors() {
  private var id: UUID = _
  private var email: String = _
  private var errorType: String = _
  private var creationDate: String = _

  def this(email: String, errorType: String, creationDate: String) = {
    this()
    this.email = email
    this.errorType = errorType
    this.creationDate = creationDate
  }

  def setId(id: UUID): Unit = {
    this.id = id
  }

  def setEmail(email: String): Unit = {
    this.email = email
  }

  def setErrorType(errorType: String): Unit = {
    this.errorType = errorType
  }

  def setCreationDate(creationDate: String): Unit = {
    this.creationDate = creationDate
  }

  def getId: UUID = id

  def getEmail: String = email

  def getErrorType: String = errorType

  def getCreationDate: String = creationDate
}
