package com.worldline.bcc.smartpush.handlers

import com.datastax.oss.driver.api.core.CqlSession
import com.worldline.bcc.smartpush.models.HistoryModelReception
import com.worldline.bcc.smartpush.services.HistoryService
import io.vertx.core.Vertx.vertx
import io.vertx.ext.web.RoutingContext
import io.vertx.core.json.Json

class HistoryHandler {

  private val cqlSession = CqlSession.builder().build()
  private val historyDAO = new HistoryService(vertx, cqlSession)

  def handleSaveHistory(context: RoutingContext): Unit = {
    val response = context.response()
    val requestBody = context.getBodyAsJson()
    if (requestBody == null) {
      response.setStatusCode(400).end("Données d'entreprise manquantes")
    } else {
      val email = requestBody.getString("email")
      val campaignId = requestBody.getInteger("campaignId")
      val contactId = requestBody.getInteger("contactId")
      val sequenceId = requestBody.getInteger("sequenceId")
      val errorCode = requestBody.getString("errorCode")
      val newHistory = HistoryModelReception(email, campaignId, contactId, sequenceId, errorCode)
      val historyAdded = historyDAO.addHistoryData(newHistory)
      response.setStatusCode(201)
      response.putHeader("content-type", "application/json")
      response.end(Json.encode(historyAdded))
    }
  }
}
