package com.worldline.bcc.smartpush.verticles

import com.worldline.bcc.smartpush.handlers.{HistoryHandler, SmtpErrorHandler, SmtpErrorTypeHandler}
import io.vertx.core.AbstractVerticle
import io.vertx.ext.web.handler.BodyHandler
import io.vertx.ext.web.Router

class HttpServerVerticle extends AbstractVerticle {
  var router = Router.router(vertx)

  override def start(): Unit = {

    router.route().handler(BodyHandler.create())

    /**
     * route de verification d'existance d'une liste d'adresse email.
     */
    router.post("/api/verify-emails").handler(new SmtpErrorHandler().handleEmailCheckRequest)

    /**
     * route de verification d'existance d'une adresse email.
     */
    router.post("/api/verify-one-email").handler(new SmtpErrorHandler().handleCheckOneEmailRequest)

    /**
     * route de verification d'existance d'une adresse email.
     */
    router.post("/smtpErrorType/verify-errorCode").handler(new SmtpErrorTypeHandler().handleErrorCodeReceptionCheckRequest)

    /**
     * route d'ajout de history  dans la BDD Cassandra.
     */
    router.post("/history/addHistory").handler(new HistoryHandler().handleSaveHistory)

    /**
     * Effacer dans la table smtp_error les lignes dont leur durée d'existance est
     */
    router.post("/api/purgeSmtpError").handler(new SmtpErrorHandler().handlePurgeRowUnlocked)


    /**
     * Route to get all SMTP error types.
     */
    router.get("/api/smtp-error-type").handler(new SmtpErrorTypeHandler().handleGetAllErrorType)


    /**
     * Route to add a new SMTP error type.
     */
    router.post("/api/smtp-error-type").handler(new SmtpErrorTypeHandler().handleSaveErrorType)

    /**
     * Route to update an existing SMTP error type by ID.
     */
    router.put("/api/smtp-error-type/:id").handler(new SmtpErrorTypeHandler().handleUpdateErrorType)

    /**
     * Route to delete an SMTP error type by ID.
     */
    router.delete("/api/smtp-error-type/:id").handler(new SmtpErrorTypeHandler().handleDeleteErrorType)

    vertx.createHttpServer()
      .requestHandler(router)
      .listen(2500)
  }
}

