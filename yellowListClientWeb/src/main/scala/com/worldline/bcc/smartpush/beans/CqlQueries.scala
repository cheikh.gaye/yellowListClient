package com.worldline.bcc.smartpush.beans

object CqlQueries {

  val insertSmtpErrorQuery: String =
    "INSERT INTO yellow_list_keyspace.smtp_error (id, email, creationDate, lastAttemptDate, nbKo, koTypeId) VALUES (?, ?, ?, ?, ?, ?)"

  val updateSmtpErrorQuery: String =
    "UPDATE yellow_list_keyspace.smtp_error SET lastAttemptDate = ?, nbKo = ? WHERE id = ? "

  val getSmtpErrorByEmailQuery: String =
    "SELECT * FROM yellow_list_keyspace.smtp_error WHERE email = ? ALLOW FILTERING"

  val getNbTotalKoOfSmtpErrorByEmailQuery: String =
    "SELECT nbko, kotypeid FROM yellow_list_keyspace.smtp_error WHERE email = ? ALLOW FILTERING"

  val getNbKoOfSmtpErrorTypeByIdQuery: String =
    "SELECT nb_ko FROM yellow_list_keyspace.smtp_error_type WHERE id = ? ALLOW FILTERING"

  val isEmailPresentInDatabaseQuery: String =
    s"SELECT COUNT(*) FROM yellow_list_keyspace.smtp_error WHERE email = ? ALLOW FILTERING"

  val insertSmtpErrorTypeQuery: String =
    "INSERT INTO yellow_list_keyspace.smtp_error_type (id, error_code_rec, nb_KO, duration, error_code_mess) VALUES (?, ?, ?, ?, ?)"

  val getSmtpErrorTypeByIdQuery: String =
    "SELECT * FROM yellow_list_keyspace.smtp_error_type WHERE id = ? ALLOW FILTERING"

  val getSmtpErrorTypeByErrorCodeReceptionQuery: String =
    "SELECT * FROM yellow_list_keyspace.smtp_error_type WHERE error_code_rec = ? ALLOW FILTERING"

  val insertHistoryDataQuery: String =
    "INSERT INTO yellow_list_keyspace.history (id, email, creationDate, campaignId, contactId,sequenceId, smtpErrorTypeId) VALUES (?, ?, ?, ?, ?, ?, ?)"

  val isErrorCodeReceptionPresentInDatabase: String =
    s"SELECT COUNT(*) FROM yellow_list_keyspace.smtp_error_type WHERE error_code_rec = ? ALLOW FILTERING"

  val getAllRow: String =
    "SELECT * FROM yellow_list_keyspace.smtp_error ALLOW FILTERING"

  val getEmailUnlockedTopurgeQuery: String =
    "SELECT id, kotypeid, lastattemptdate  FROM yellow_list_keyspace.smtp_error ALLOW FILTERING"

    val purgeSmtpErrorByIdQuery: String =
      "DELETE FROM yellow_list_keyspace.smtp_error WHERE id = ?"

  val getSmtpErrorTypeAll: String =
    "SELECT * FROM yellow_list_keyspace.smtp_error_type ALLOW FILTERING"

  val updateSmtpErrorTypeQuery: String =
    "UPDATE yellow_list_keyspace.smtp_error_type SET error_code_rec = ?, nb_KO = ?, duration = ?, error_code_mess = ? WHERE id = ?"

  val deleteSmtpErrorTypeQuery : String =
    "DELETE FROM yellow_list_keyspace.smtp_error_type WHERE id = ?"
}
