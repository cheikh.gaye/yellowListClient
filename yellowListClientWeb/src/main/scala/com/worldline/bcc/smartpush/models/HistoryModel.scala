package com.worldline.bcc.smartpush.models

import java.util.UUID

case class HistoryModel(id: UUID, email: String, creationDate: String, campaignId: Int, contactId: Int, sequenceId: Int, koTypeId: UUID)
case class HistoryModelReception(email: String, campaignId: Int, contactId: Int, sequenceId: Int, errorCode: String)
