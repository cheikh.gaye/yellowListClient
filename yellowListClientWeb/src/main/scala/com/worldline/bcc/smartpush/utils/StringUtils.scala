package com.worldline.bcc.smartpush.utils

import io.vertx.core.Vertx
import io.vertx.ext.web.client.{WebClient, WebClientOptions}

object StringUtils {

  /**
   * Extraire le nom de domaine d'une adresse email
   *
   * @param args adresse email
   * @return nom de domaine
   */
  def emailExtractorDomainName(args: String) = {

    // Regular expression pattern to extract @gmail.com
    val pattern = "@([a-zA-Z0-9.-]+)".r

    // Match the pattern in the email string
    val result = pattern.findFirstMatchIn(args) match {
      case Some(matched) => matched.group(0) // Extract the matched part
      case None => "No match found"
    }

    result
  }
}
