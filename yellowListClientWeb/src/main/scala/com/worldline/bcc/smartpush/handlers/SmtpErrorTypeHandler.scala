package com.worldline.bcc.smartpush.handlers

import com.datastax.oss.driver.api.core.CqlSession
import com.worldline.bcc.smartpush.models.SmtpErrorTypeModel
import com.worldline.bcc.smartpush.services.SmtpErrorTypeService
import io.vertx.core.json.Json
import io.vertx.ext.web.RoutingContext
import io.vertx.core.Vertx.vertx

import java.util.UUID

class SmtpErrorTypeHandler {

  private val cqlSession = CqlSession.builder().build()
  private val smtpErrorTypeService = new SmtpErrorTypeService(vertx, cqlSession)

  def handleErrorCodeReceptionCheckRequest(context: RoutingContext): Unit = {
    val errorCode = context.getBodyAsString
    if (errorCode != null) {
      val isEmailsBlocked = smtpErrorTypeService.isErrorCodeReceptionPresentInDatabase(errorCode)
      context.response()
        .putHeader("content-type", "application/json")
        .end(isEmailsBlocked.toString)
    } else {
      context.response()
        .setStatusCode(400)
        .end("Invalid JSON in the request body")
    }
  }

  def handleSaveErrorType(context: RoutingContext): Unit = {
    val response = context.response()
    val requestBody = context.getBodyAsJson()
    if (requestBody == null) {
      response.setStatusCode(400).end("Missing request body")
    } else {
      try {
        val errorCodeRec = requestBody.getString("errorCodeRec")
        val nbKO = requestBody.getInteger("nbKO")
        val duration = requestBody.getInteger("duration")
        val errorCodeMess = requestBody.getString("errorCodeMess")
        val newErrorType = SmtpErrorTypeModel(UUID.randomUUID(), errorCodeRec, nbKO, duration, errorCodeMess)
        val savedErrorType = smtpErrorTypeService.addSmtpErrorType(newErrorType)
        response.setStatusCode(201)
          .putHeader("content-type", "application/json")
          .end(Json.encode(savedErrorType))
      } catch {
        case e: Exception =>
          response.setStatusCode(500)
            .end("Error while saving SmtpErrorType: " + e.getMessage)
      }
    }
  }

  def handleGetAllErrorType(context: RoutingContext): Unit = {
    val response = context.response()
    try {
      val allErrorTypes = smtpErrorTypeService.getAllSmtpErrorTypes
      response.setStatusCode(200)
        .putHeader("content-type", "application/json")
        .end(Json.encode(allErrorTypes))
    } catch {
      case e: Exception =>
        response.setStatusCode(500)
          .end("Error while fetching all SmtpErrorTypes: " + e.getMessage)
    }
  }

  def handleUpdateErrorType(context: RoutingContext): Unit = {
    val response = context.response()
    val requestBody = context.getBodyAsJson()
    if (requestBody == null) {
      response.setStatusCode(400).end("Missing request body")
    } else {
      try {
        val id = UUID.fromString(requestBody.getString("id"))
        val errorCodeRec = requestBody.getString("errorCodeRec")
        val nbKO = requestBody.getInteger("nbKO")
        val duration = requestBody.getInteger("duration")
        val errorCodeMess = requestBody.getString("errorCodeMess")
        val updatedErrorType = SmtpErrorTypeModel(id, errorCodeRec, nbKO, duration, errorCodeMess)
        val updated = smtpErrorTypeService.updateSmtpErrorType(updatedErrorType)
        if (updated.succeeded()) {
          response.setStatusCode(200).end("SmtpErrorType updated successfully")
        } else {
          response.setStatusCode(404).end("SmtpErrorType not found")
        }
      } catch {
        case e: IllegalArgumentException =>
          response.setStatusCode(400).end("Invalid UUID provided")
        case e: Exception =>
          response.setStatusCode(500).end("Error while updating SmtpErrorType: " + e.getMessage)
      }
    }
  }

  def handleDeleteErrorType(context: RoutingContext): Unit = {
    val response = context.response()
    val id = context.request().getParam("id")
    if (id == null) {
      response.setStatusCode(400).end("Missing SmtpErrorType ID")
    } else {
      try {
        val uuid = UUID.fromString(id)
        val deleted = smtpErrorTypeService.deleteSmtpErrorType(uuid)
        if (deleted.succeeded()) {
          response.setStatusCode(204).end()
        } else {
          response.setStatusCode(404).end("SmtpErrorType not found")
        }
      } catch {
        case e: IllegalArgumentException =>
          response.setStatusCode(400).end("Invalid UUID provided")
        case e: Exception =>
          response.setStatusCode(500).end("Error while deleting SmtpErrorType: " + e.getMessage)
      }
    }
  }
}
