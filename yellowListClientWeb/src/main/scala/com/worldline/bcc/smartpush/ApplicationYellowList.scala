package com.worldline.bcc.smartpush


import com.worldline.bcc.smartpush.Schedules.PurgeSmtpError
import com.worldline.bcc.smartpush.verticles.HttpServerVerticle
import com.worldline.bcc.smartpush.producer.KafkaProducerApp
import io.vertx.core.{AbstractVerticle, Vertx}

import java.util.UUID
//import com.worldline.bcc.smartpush.producer.KafkaProducerApp
import com.worldline.bcc.smartpush.smtpService.SMTPListner
import io.vertx.ext.web.Router

object ApplicationYellowList {
  def main(args: Array[String]): Unit = {
    val vertx = Vertx.vertx()
    val server = vertx.createHttpServer()
    val router = Router.router(vertx)
    //******************* Run consumer
//    SMTPListner.run()
//    KafkaProducerApp.runProducer()

    //******************* deploy the APIs
    vertx.deployVerticle(new HttpServerVerticle)

    //******************* Schedule to launch
    PurgeSmtpError.launchPurge()

    router.route("/").handler(ctx => {
      ctx.response().end("Hello, Vert.x Scala!")
    })

    server.requestHandler(router).listen(2500)
  }

}
