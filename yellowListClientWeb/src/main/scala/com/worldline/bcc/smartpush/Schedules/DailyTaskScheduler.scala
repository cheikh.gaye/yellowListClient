package com.worldline.bcc.smartpush.Schedules

import com.worldline.bcc.smartpush.handlers.SmtpErrorHandler
import io.vertx.core.Vertx

import java.time.LocalTime
import java.util.concurrent.TimeUnit

object DailyTaskScheduler {

  def main(args: Array[String]): Unit = {
    val vertx = Vertx.vertx()

    // Define the time of day to run the task (2 am)
    val scheduledTime = LocalTime.of(2, 0)

    // Calculate the initial delay in milliseconds until the next occurrence
    val now = LocalTime.now()
    val initialDelayMillis = calculateInitialDelayMillis(now, scheduledTime)

    // Define the task to be executed
    val task = new Runnable {
      def run(): Unit = {
        // Your task logic here
        println("Task executed at " + LocalTime.now())
      }
    }

    // Schedule the task to run every day at the specified time
    val delayMillis = TimeUnit.DAYS.toMillis(1) // 24 hours
    vertx.setTimer(initialDelayMillis, _ => {
      task.run()
      // Schedule the task to run every day at the same time
      new SmtpErrorHandler().purgeSmtpError()
      vertx.setPeriodic(delayMillis, _ => {
        task.run()
      })
    })

    // Optionally, you can also deploy Verticles and start your Vert.x application here.
  }

  def calculateInitialDelayMillis(now: LocalTime, scheduledTime: LocalTime): Long = {
    val nowMillis = now.toSecondOfDay * 1000
    val scheduledTimeMillis = scheduledTime.toSecondOfDay * 1000
    val initialDelayMillis = if (nowMillis <= scheduledTimeMillis) {
      scheduledTimeMillis - nowMillis
    } else {
      TimeUnit.DAYS.toMillis(1) - (nowMillis - scheduledTimeMillis)
    }
    initialDelayMillis
  }
}
