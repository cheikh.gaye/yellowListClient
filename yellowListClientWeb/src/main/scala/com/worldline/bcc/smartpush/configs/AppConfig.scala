package com.worldline.bcc.smartpush.configs

import com.typesafe.config.ConfigFactory

object AppConfig {
  private val config = ConfigFactory.load()
  // Cassandra
  val cassandraHost: String = config.getString("cassandra.host")
  val cassandraPort: Int = config.getInt("cassandra.port")
  val cassandraUsername: String = config.getString("cassandra.username")
  val cassandraPassword: String = config.getString("cassandra.password")
  val cassandraKeyspace: String = config.getString("cassandra.keyspace")
  val cassandraLocalDatacenter: String = config.getString("cassandra.local-datacenter")
  
  // Kafka
  val kafkaTopic : String = config.getString("kafka.topic")
  val consumerGroup: String = config.getString("kafka.consumer-group")
  val kafkaBrockerAddress : String = config.getString("kafka.brocker-address")

  // Add more configuration properties as needed
}