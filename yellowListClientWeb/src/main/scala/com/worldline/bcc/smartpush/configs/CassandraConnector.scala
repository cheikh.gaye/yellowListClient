package com.worldline.bcc.smartpush.configs

import com.datastax.oss.driver.api.core.CqlSession
object CassandraConnector {
  private val session = CqlSession.builder()
    .withKeyspace(AppConfig.cassandraKeyspace)
//    .addContactPoint(AppConfig.cassandraHost)
    .withLocalDatacenter(AppConfig.cassandraLocalDatacenter)
    .withAuthCredentials(AppConfig.cassandraUsername, AppConfig.cassandraPassword)
    .build()

  def getSession: CqlSession = session
}
