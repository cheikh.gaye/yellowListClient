package com.worldline.bcc.smartpush.models

import java.util.{Date, UUID}

case class SmtpErrorModel(id: UUID, email: String, creationDate: String, lastAttemptDate: String, nbKo: Int, koTypeId: UUID)

case class SmtpErrorPurgeModel(id: UUID, lastAttemptDate: String, koTypeId: UUID)
