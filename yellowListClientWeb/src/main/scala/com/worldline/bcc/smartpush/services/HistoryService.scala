package com.worldline.bcc.smartpush.services

import com.datastax.oss.driver.api.core.CqlSession
import com.datastax.oss.driver.api.core.cql.{BoundStatement, PreparedStatement}
import com.worldline.bcc.smartpush.beans.CqlQueries
import com.worldline.bcc.smartpush.models.{HistoryModel, HistoryModelReception}
import com.worldline.bcc.smartpush.utils.DateUtils
import io.vertx.core.{Future, Vertx}

import java.time.LocalDateTime
import java.util.UUID

class HistoryService(vertx: Vertx, cqlSession: CqlSession){
  private val insertHistoryDataQuery: PreparedStatement = cqlSession.prepare(CqlQueries.insertHistoryDataQuery)
  private val getSmtpErrorTypeByErrorCodeReceptionQuery: PreparedStatement = cqlSession.prepare(CqlQueries.getSmtpErrorTypeByErrorCodeReceptionQuery)

  def addHistoryData(modelReception: HistoryModelReception): Future[Unit] = {
    val boundStatement0: BoundStatement = getSmtpErrorTypeByErrorCodeReceptionQuery.bind(modelReception.errorCode)
    val resultSet0 = cqlSession.execute(boundStatement0)
    val row = resultSet0.one()
    if (row != null) {
      val smtpErrorTypeId = row.getUuid("id")
      val boundStatement1: BoundStatement = insertHistoryDataQuery.bind(
        UUID.randomUUID(),
        modelReception.email,
        DateUtils.convertLocalDateToString(LocalDateTime.now()),
        modelReception.campaignId,
        modelReception.contactId,
        modelReception.sequenceId,
        smtpErrorTypeId
      )
      Future.future((promise: io.vertx.core.Promise[Unit]) => {
          cqlSession.executeAsync(boundStatement1).whenComplete((_, throwable) => {
            if (throwable != null) {
              promise.fail(throwable)
            } else {
              promise.complete(())
            }
          })
      })
    } else {
      throw new Exception("errorCode is not accepted")
    }
  }
}
