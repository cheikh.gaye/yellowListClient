package com.worldline.bcc.smartpush.services

import com.datastax.oss.driver.api.core.CqlSession
import com.datastax.oss.driver.api.core.cql.{AsyncResultSet, BoundStatement, PreparedStatement, Row}
import com.worldline.bcc.smartpush.beans.CqlQueries
import com.worldline.bcc.smartpush.models.SmtpErrorTypeModel
import io.vertx.core.{Future, Promise, Vertx}
import java.util.UUID
import scala.jdk.CollectionConverters._

class SmtpErrorTypeService(vertx: Vertx, cqlSession: CqlSession) {

  private val insertSmtpErrorTypeQuery: PreparedStatement = cqlSession.prepare(CqlQueries.insertSmtpErrorTypeQuery)
  private val getSmtpErrorTypeByIdQuery: PreparedStatement = cqlSession.prepare(CqlQueries.getSmtpErrorTypeByIdQuery)
  private val getSmtpErrorTypeByErrorCodeReceptionQuery: PreparedStatement = cqlSession.prepare(CqlQueries.getSmtpErrorTypeByErrorCodeReceptionQuery)
  private val isErrorCodeReceptionPresentInDatabase: PreparedStatement = cqlSession.prepare(CqlQueries.isErrorCodeReceptionPresentInDatabase)
  private val getSmtpErrorTypeAll: PreparedStatement = cqlSession.prepare(CqlQueries.getSmtpErrorTypeAll)
  private val updateSmtpErrorTypeQuery: PreparedStatement = cqlSession.prepare(CqlQueries.updateSmtpErrorTypeQuery)
  private val deleteSmtpErrorTypeQuery: PreparedStatement = cqlSession.prepare(CqlQueries.deleteSmtpErrorTypeQuery)

  def addSmtpErrorType(smtpErrorTypeModel: SmtpErrorTypeModel): Future[Void] = {
    val boundStatement: BoundStatement = insertSmtpErrorTypeQuery.bind(
      smtpErrorTypeModel.id, smtpErrorTypeModel.error_code_rec, smtpErrorTypeModel.nb_KO,
      smtpErrorTypeModel.duration, smtpErrorTypeModel.error_code_mess
    )
    val promise: Promise[Void] = Promise.promise()

    cqlSession.executeAsync(boundStatement).whenComplete((_, throwable) => {
      if (throwable != null) {
        promise.fail(throwable)
      } else {
        promise.complete()
      }
    })
    promise.future()
  }

  def isErrorCodeReceptionPresentInDatabase(errorCode: String): Boolean = {
    val boundStatement: BoundStatement = isErrorCodeReceptionPresentInDatabase.bind(errorCode)
    val resultSet = cqlSession.execute(boundStatement)
    val row = resultSet.one()
    val count = row.getLong(0)
    count > 0
  }

  def findById(id: UUID): Future[Option[SmtpErrorTypeModel]] = {
    val boundStatement: BoundStatement = getSmtpErrorTypeByIdQuery.bind(id)
    val promise: Promise[Option[SmtpErrorTypeModel]] = Promise.promise()

    cqlSession.executeAsync(boundStatement).whenComplete((resultSet, throwable) => {
      if (throwable != null) {
        promise.fail(throwable)
      } else {
        val row = resultSet.one()
        if (row != null) {
          val smtpErrorType = Some(SmtpErrorTypeModel(
            id = row.getUuid("id"),
            error_code_rec = row.getString("error_code_rec"),
            nb_KO = row.getInt("nb_KO"),
            duration = row.getInt("duration"),
            error_code_mess = row.getString("error_code_mess")
          ))
          promise.complete(smtpErrorType)
        } else {
          promise.complete(None)
        }
      }
    })
    promise.future()
  }

  def findByErrorCodeReception(error_code_rec: String): Future[Option[SmtpErrorTypeModel]] = {
    val boundStatement: BoundStatement = getSmtpErrorTypeByErrorCodeReceptionQuery.bind(error_code_rec)
    val promise: Promise[Option[SmtpErrorTypeModel]] = Promise.promise()

    cqlSession.executeAsync(boundStatement).whenComplete((resultSet, throwable) => {
      if (throwable != null) {
        promise.fail(throwable)
      } else {
        val row = resultSet.one()
        if (row != null) {
          val smtpErrorType = Some(SmtpErrorTypeModel(
            id = row.getUuid("id"),
            error_code_rec = row.getString("error_code_rec"),
            nb_KO = row.getInt("nb_KO"),
            duration = row.getInt("duration"),
            error_code_mess = row.getString("error_code_mess")
          ))
          promise.complete(smtpErrorType)
        } else {
          promise.complete(None)
        }
      }
    })
    promise.future()
  }

  def getAllSmtpErrorTypes(): Future[List[SmtpErrorTypeModel]] = {
    val boundStatement: BoundStatement = getSmtpErrorTypeAll.bind()
    val promise: Promise[List[SmtpErrorTypeModel]] = Promise.promise()

    cqlSession.executeAsync(boundStatement).whenComplete((resultSet, throwable) => {
      if (throwable != null) {
        promise.fail(throwable)
      } else {
        val results = scala.collection.mutable.ListBuffer[SmtpErrorTypeModel]()

        def processResultSet(asyncResultSet: AsyncResultSet): Unit = {
          asyncResultSet.currentPage().iterator().asScala.foreach { row: Row =>
            results += SmtpErrorTypeModel(
              id = row.getUuid("id"),
              error_code_rec = row.getString("error_code_rec"),
              nb_KO = row.getInt("nb_ko"),
              duration = row.getInt("duration"),
              error_code_mess = row.getString("error_code_mess")
            )
          }

          if (asyncResultSet.hasMorePages) {
            asyncResultSet.fetchNextPage().whenComplete((nextResultSet, nextThrowable) => {
              if (nextThrowable != null) {
                promise.fail(nextThrowable)
              } else {
                processResultSet(nextResultSet)
              }
            })
          } else {
            promise.complete(results.toList)
          }
        }

        processResultSet(resultSet)
      }
    })
    promise.future()
  }

  def updateSmtpErrorType(smtpErrorTypeModel: SmtpErrorTypeModel): Future[Void] = {
    val boundStatement: BoundStatement = updateSmtpErrorTypeQuery.bind(
      smtpErrorTypeModel.error_code_rec, smtpErrorTypeModel.nb_KO, smtpErrorTypeModel.duration,
      smtpErrorTypeModel.error_code_mess, smtpErrorTypeModel.id
    )
    val promise: Promise[Void] = Promise.promise()

    cqlSession.executeAsync(boundStatement).whenComplete((_, throwable) => {
      if (throwable != null) {
        promise.fail(throwable)
      } else {
        promise.complete()
      }
    })
    promise.future()
  }

  def deleteSmtpErrorType(id: UUID): Future[Void] = {
    val boundStatement: BoundStatement = deleteSmtpErrorTypeQuery.bind(id)
    val promise: Promise[Void] = Promise.promise()

    cqlSession.executeAsync(boundStatement).whenComplete((_, throwable) => {
      if (throwable != null) {
        promise.fail(throwable)
      } else {
        promise.complete()
      }
    })
    promise.future()
  }
}
