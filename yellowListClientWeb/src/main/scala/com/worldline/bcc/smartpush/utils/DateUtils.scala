package com.worldline.bcc.smartpush.utils

import io.vertx.scala.core.Vertx
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
object DateUtils {

  /**
   * convertun localDateTime en date format texte
   *
   * @param localDateTime
   * @return Format Date yyyy-MM-dd HH:mm:ss
   */
  def convertLocalDateToString(localDateTime: LocalDateTime): String = {
    // Define a DateTimeFormatter for the desired format
    val formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")
    // Format the LocalDateTime to a string
    val formattedDateTime = localDateTime.format(formatter)

    formattedDateTime.toString
  }

  def addDayToDateTimeString(datetimeString: String, duration: Int): LocalDateTime = {
    //val datetimeString = "2023-11-02 12:18:30"
    val formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")

    val parsedDateTime = LocalDateTime.parse(datetimeString, formatter)

    val newDateTime = parsedDateTime.plusDays(duration)

    val newDateTimeString = newDateTime.format(formatter)

    LocalDateTime.parse(newDateTimeString, formatter)
  }
}
