package com.worldline.bcc.smartpush.Schedules

import com.worldline.bcc.smartpush.handlers.SmtpErrorHandler
import io.vertx.core.Vertx

import java.time.LocalTime
import java.util.concurrent.TimeUnit

object PurgeSmtpError {

  def launchPurge(): Unit = {
    val vertx = Vertx.vertx()
    
    val scheduledTime = LocalTime.of(2, 0)
    
    val now = LocalTime.now()
    val initialDelayMillis = calculateInitialDelayMillis(now, scheduledTime)

    val task = new Runnable {
      def run(): Unit = {
        // Request to purge SMTP ERRORS
        new SmtpErrorHandler().purgeSmtpError()
        println("Suppression executer à " + LocalTime.now())
      }
    }

    // Schedule the task to run every day at the specified time
    val delayMillis = TimeUnit.DAYS.toMillis(1) 
    vertx.setTimer(initialDelayMillis, _ => {
      task.run()
     
      vertx.setPeriodic(delayMillis, _ => {
        task.run()
      })
    })


  }

  def calculateInitialDelayMillis(now: LocalTime, scheduledTime: LocalTime): Long = {
    val nowMillis = now.toSecondOfDay * 1000
    val scheduledTimeMillis = scheduledTime.toSecondOfDay * 1000
    val initialDelayMillis = if (nowMillis <= scheduledTimeMillis) {
      scheduledTimeMillis - nowMillis
    } else {
      TimeUnit.DAYS.toMillis(1) - (nowMillis - scheduledTimeMillis)
    }
    initialDelayMillis
  }

}
