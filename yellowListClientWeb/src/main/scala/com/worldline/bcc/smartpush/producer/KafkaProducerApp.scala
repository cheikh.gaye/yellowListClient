package com.worldline.bcc.smartpush.producer

import com.worldline.bcc.smartpush.configs.AppConfig

import java.util.Properties
import org.apache.kafka.clients.producer.{KafkaProducer, ProducerConfig, ProducerRecord}

/**
 * Objet à appeler pour tester le consumer SMTPListner
 */
object KafkaProducerApp {
  /**
   * demarrage du producer
   */
  def runProducer() = {
    val kafkaProps = new Properties()
    kafkaProps.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, AppConfig.kafkaBrockerAddress)
    kafkaProps.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer")
    kafkaProps.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer")
    val producer = new KafkaProducer[String, String](kafkaProps)

    // Create and send a Kafka message
    val topic = AppConfig.kafkaTopic
    val email = "yellowlist@yopmail.com"
    val error = "702"

    val record = new ProducerRecord[String, String](topic, email, error)
    producer.send(record)
    producer.close()
  }
}
