package com.worldline.bcc.smartpush.handlers

import com.datastax.oss.driver.api.core.CqlSession
import com.worldline.bcc.smartpush.services.SmtpErrorService
import io.vertx.core.Vertx.vertx
import io.vertx.core.json.{JsonArray, JsonObject}
import io.vertx.ext.web.RoutingContext
import io.vertx.ext.web.Router
import io.vertx.core.http.HttpServerResponse
import io.vertx.ext.web.RoutingContext

import scala.concurrent.Future
import scala.util.{Failure, Success}

class SmtpErrorHandler {

  private val cqlSession = CqlSession.builder().build()
  private val smtpErrorDAO = new SmtpErrorService(vertx, cqlSession)

  /**
   * verification d'exstance d'une adresse email
   *
   * @param context
   */
  def handleCheckOneEmailRequest(context: RoutingContext): Unit = {
    val email = context.getBodyAsString
    if (email != null) {
      val isEmailBlocked = smtpErrorDAO.isEmailBlocked(email)
      context.response()
        .putHeader("content-type", "application/json")
        .end(isEmailBlocked.toString)
    } else {
      context.response()
        .setStatusCode(400)
        .end("Invalid JSON in the request body")
    }
  }

  /**
   * Verifier l'existance de plusieurs adresse email
   *
   * @param context
   */
  def handleEmailCheckRequest(context: RoutingContext): Unit = {
    val emailList = context.getBodyAsJsonArray
    val resultArray = new JsonArray()
    if (emailList != null) {
      emailList.getList.forEach { field =>
        val isEmailsBlocked = smtpErrorDAO.isEmailBlocked(field.toString)
        resultArray.add(isEmailsBlocked)
      }
      context.response()
        .putHeader("content-type", "application/json")
        .end(resultArray.encode())
    } else {
      context.response()
        .setStatusCode(400)
        .end("Invalid JSON in the request body")
    }
  }

  /**
   * Endpoint pour supprimer les adresses emails atteingnant la durée de purge
   *
   * @param context
   */
  def handlePurgeRowUnlocked(context: RoutingContext): Unit = {
    val smtpErrorList = smtpErrorDAO.getPurgeSmtpError()
    val jsonArray = new JsonArray()

    smtpErrorList.foreach { smtpError =>
      val jsonObject = new JsonObject()
        .put("id", smtpError.id.toString)
        .put("lastAttemptDate", smtpError.lastAttemptDate)
        .put("koTypeId", smtpError.koTypeId.toString)
      jsonArray.add(jsonObject)
      smtpErrorDAO.deleteSmtpErrors(smtpError.id)
    }
    val response: HttpServerResponse = context.response()
    response.putHeader("content-type", "application/json")
    response.end(jsonArray.encode())
  }

  /**
   * supprimer les adresses emails atteingnant la durée de purge
   *
   */
  def purgeSmtpError(): Unit = {
    val smtpErrorList = smtpErrorDAO.getPurgeSmtpError()
    val jsonArray = new JsonArray()

    smtpErrorList.foreach { smtpError =>
      val jsonObject = new JsonObject()
        .put("id", smtpError.id.toString)
        .put("lastAttemptDate", smtpError.lastAttemptDate)
        .put("koTypeId", smtpError.koTypeId.toString)
      jsonArray.add(jsonObject)
      smtpErrorDAO.deleteSmtpErrors(smtpError.id)
    }
  }




}
