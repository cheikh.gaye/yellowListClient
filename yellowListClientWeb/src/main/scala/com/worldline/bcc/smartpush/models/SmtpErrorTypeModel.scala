package com.worldline.bcc.smartpush.models

import java.util.UUID

case class SmtpErrorTypeModel(id: UUID, error_code_rec: String, nb_KO: Int, duration: Int, error_code_mess: String)
