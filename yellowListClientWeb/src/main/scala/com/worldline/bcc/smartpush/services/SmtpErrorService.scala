package com.worldline.bcc.smartpush.services

import com.datastax.oss.driver.api.core.CqlSession
import com.datastax.oss.driver.api.core.cql.{BoundStatement, PreparedStatement, ResultSet, Row}
import com.worldline.bcc.smartpush.beans.CqlQueries
import com.worldline.bcc.smartpush.models.{SmtpErrorModel, SmtpErrorPurgeModel, SmtpErrorTypeModel}
import com.worldline.bcc.smartpush.smtpService.SMTPListner.{smtpErrorTypeDAO, vertx}
import com.worldline.bcc.smartpush.utils.DateUtils.addDayToDateTimeString
import io.vertx.core.{Future, Vertx}

import java.time.LocalDateTime
import java.util.UUID
import java.util.stream.Collectors
import scala.language.postfixOps

class SmtpErrorService(vertx: Vertx, cqlSession: CqlSession) {
  private var smtpErrorTypeService: SmtpErrorTypeService = _
  smtpErrorTypeService = new SmtpErrorTypeService(vertx, cqlSession)

  private val insertSmtpErrorQuery: PreparedStatement = cqlSession.prepare(CqlQueries.insertSmtpErrorQuery)
  private val updateSmtpErrorQuery: PreparedStatement = cqlSession.prepare(CqlQueries.updateSmtpErrorQuery)
  private val getSmtpErrorByEmailQuery: PreparedStatement = cqlSession.prepare(CqlQueries.getSmtpErrorByEmailQuery)
  private val isEmailPresentInDatabaseQuery: PreparedStatement = cqlSession.prepare(CqlQueries.isEmailPresentInDatabaseQuery)
  private val getNbTotalKoOfSmtpErrorByEmailQuery: PreparedStatement = cqlSession.prepare(CqlQueries.getNbTotalKoOfSmtpErrorByEmailQuery)
  private val getNbKoOfSmtpErrorTypeByIdQuery: PreparedStatement = cqlSession.prepare(CqlQueries.getNbKoOfSmtpErrorTypeByIdQuery)
  private val getEmailUnlockedTopurgeQuery: PreparedStatement = cqlSession.prepare(CqlQueries.getEmailUnlockedTopurgeQuery)
  private val purgeSmtpErrorByIdQuery: PreparedStatement = cqlSession.prepare(CqlQueries.purgeSmtpErrorByIdQuery)

  def addSmtpError(smtpErrorModel: SmtpErrorModel): Future[Unit] = {
    val boundStatement: BoundStatement = insertSmtpErrorQuery.bind(smtpErrorModel.id, smtpErrorModel.email, smtpErrorModel.creationDate, smtpErrorModel.lastAttemptDate, smtpErrorModel.nbKo, smtpErrorModel.koTypeId)
    Future
      .future((promise: io.vertx.core.Promise[Unit]) => {
        cqlSession.executeAsync(boundStatement).whenComplete((_, throwable) => {
          if (throwable != null) {
            promise.fail(throwable)
          } else {
            promise.complete(())
          }
        })
      })
  }

  def getSmtpErrorByEmail(email: String): Option[SmtpErrorModel] = {
    val boundStatement = getSmtpErrorByEmailQuery.bind(email)
    val resultSet = cqlSession.execute(boundStatement)
    val row = resultSet.one()
    if (row != null) {
      Some(
        SmtpErrorModel(
          id = row.getUuid("id"),
          email = row.getString("email"),
          creationDate = row.getString("creationDate"),
          lastAttemptDate = row.getString("lastAttemptDate"),
          nbKo = row.getInt("nbKo"),
          koTypeId = row.getUuid("koTypeId")
        )
      )
    } else {
      None // email not found
    }
  }

  def UpdateSmtpError(smtpErrorModel: SmtpErrorModel): Future[Unit] = {
    val boundStatement: BoundStatement = updateSmtpErrorQuery.bind(smtpErrorModel.lastAttemptDate, smtpErrorModel.nbKo, smtpErrorModel.id)
    Future
      .future((promise: io.vertx.core.Promise[Unit]) => {
        cqlSession.executeAsync(boundStatement).whenComplete((_, throwable) => {
          if (throwable != null) {
            promise.fail(throwable)
          } else {
            promise.complete(())
          }
        })
      })
  }
  def isEmailPresentInDatabase(email: String): Boolean = {
    val boundStatement: BoundStatement = isEmailPresentInDatabaseQuery.bind(email)
    val resultSet = cqlSession.execute(boundStatement)
    val row = resultSet.one()
    val count = row.getLong(0)
    count > 0
  }

  def isEmailBlocked(email: String): Boolean = {
    val boundStatement1: BoundStatement = getNbTotalKoOfSmtpErrorByEmailQuery.bind(email)
    val resultSet1 = cqlSession.execute(boundStatement1)
    val row1 = resultSet1.one()
    if (row1 != null) {
      val nbTotalKo = row1.getInt("nbko")
      val koTypeId = row1.getUuid("kotypeid")
      val boundStatement2: BoundStatement = getNbKoOfSmtpErrorTypeByIdQuery.bind(koTypeId)
      val resultSet2 = cqlSession.execute(boundStatement2)
      val row2 = resultSet2.one()
      if (row2 != null) {
        val nbKo = row2.getInt("nb_ko")
        return nbTotalKo >= nbKo
      }
    }
    false
  }


  def getPurgeSmtpError(): List[SmtpErrorPurgeModel] = {
    val boundStatement: BoundStatement = getEmailUnlockedTopurgeQuery.bind()
    var smtpErrorList: List[SmtpErrorPurgeModel] = List.empty[SmtpErrorPurgeModel] // Use a var for mutable list
    val resultSet = cqlSession.execute(boundStatement)

    import scala.collection.JavaConverters._

    val result: List[SmtpErrorPurgeModel] = resultSet.all().asScala.toList
      .map { row =>
        val id = row.getUuid("id")
        val lastattemptdate = row.getString("lastattemptdate")
        val kotypeid = row.getUuid("kotypeid")
        SmtpErrorPurgeModel(id, lastattemptdate, kotypeid)
      }

    result.foreach { smtpError =>
      // val smtpErrorType: Option[SmtpErrorTypeModel] = smtpErrorTypeService.findById(UUID.fromString("9952b5e8-7deb-46e6-a0d5-551c40af1ac0"))
      val smtpErrorType: Future[Option[SmtpErrorTypeModel]] = smtpErrorTypeService.findById(smtpError.koTypeId)
      if (smtpErrorType.succeeded()) {
        if (LocalDateTime.now().isAfter(addDayToDateTimeString(smtpError.lastAttemptDate, 4))) {
          smtpErrorList = smtpErrorList :+ smtpError
        }
      }
    }
    smtpErrorList
  }

  def deleteSmtpErrors(id: UUID): Unit = {
    val boundDeleteStatement: BoundStatement = purgeSmtpErrorByIdQuery.bind(id)
    cqlSession.execute(boundDeleteStatement)
  }


}
